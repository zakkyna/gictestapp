import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gictestapp/ui/pages/pages.dart';
import 'package:provider/provider.dart';
import 'package:gictestapp/locator.dart';
import 'package:gictestapp/services/services.dart';
import 'package:gictestapp/viewmodels/viewmodels.dart';

void main() async {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => locator<BaseVM>(),
        ),
        ChangeNotifierProvider(
          create: (_) => locator<ContactVM>(),
        ),
      ],
      child: MaterialApp(
        title: 'GIC Test App',
        navigatorKey: locator<NavigationService>().navigatorKey,
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomePage(),
      ),
    );
  }
}
