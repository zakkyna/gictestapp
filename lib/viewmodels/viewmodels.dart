import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:gictestapp/ui/widgets/widgets.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:gictestapp/locator.dart';
import 'package:gictestapp/services/services.dart';
import 'package:gictestapp/models/models.dart';

part 'base_vm.dart';
part 'contact_vm.dart';
