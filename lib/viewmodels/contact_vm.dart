part of 'viewmodels.dart';

class ContactVM extends BaseVM {
  final APIService _apiService = locator<APIService>();

  int countOfPages = 4;
  bool _isError = false;
  bool get isError => _isError;
  set isError(bool value) {
    _isError = value;
    notifyListeners();
  }

  List<Contact> _listContact;
  List<Contact> get listContact => _listContact;

  int _page = 1;
  int get page => _page;
  set page(int value) {
    _page = value;
    notifyListeners();
  }

  int _endPage;
  int get endPage => _endPage;
  set endPage(int value) {
    _endPage = value;
    notifyListeners();
  }

  Future getContactList(BuildContext context) async {
    List<Contact> _list = await _apiService.getContactList(
          start: '0',
          limit: '$countOfPages',
          emptyCallback: (message) => showSnackBar(context, text: message),
          errorCallback: (message) => showSnackBar(context, text: message),
        ) ??
        [];
    _listContact = _list ?? [];
    if (((_list?.length ?? 0) < countOfPages) && _list.length != 0) {
      endPage = 1;
    }
    notifyListeners();
    return;
  }

  Future onLoadNextContact(BuildContext context) async {
    if (isError) {
      isError = false;
    }
    if (endPage == null) {
      page = page + 1;
    }
    int _start = page * countOfPages - (countOfPages - 1);
    int _limit = page * countOfPages;

    print('start = $_start');
    print('limit = $_limit');

    List<Contact> _list = await _apiService.getContactList(
          start: '$_start',
          limit: '$_limit',
          emptyCallback: (message) => showSnackBar(context, text: message),
          errorCallback: (message) => showSnackBar(context, text: message),
        ) ??
        [];

    if (_list != null) {
      _listContact?.addAll(_list);
      final ids = _listContact?.map((e) => e.id)?.toSet();
      _listContact?.retainWhere((x) => ids.remove(x.id));

      if (_list.length == 0 || _list.length < countOfPages) {
        endPage = page;
      }
    }
    notifyListeners();
    return;
  }

  Future onRefreshList(BuildContext context) async {
    _listContact = null;
    notifyListeners();
    endPage = null;
    page = 1;
    _listContact = await _apiService.getContactList(
          start: '0',
          limit: '$countOfPages',
          emptyCallback: (message) => showSnackBar(context, text: message),
          errorCallback: (message) => showSnackBar(context, text: message),
        ) ??
        [];
    notifyListeners();
    return;
  }

  Future<Contact> getContact(BuildContext context, int contactId) async {
    return await _apiService.getContact(
      contactId: contactId,
      emptyCallback: (message) => showSnackBar(context, text: message),
      errorCallback: (message) => showSnackBar(context, text: message),
    );
  }

  Future<Contact> addContact(BuildContext context, Contact contact) async {
    Contact _contact = await _apiService.addContact(
      contact: contact,
      emptyCallback: (message) => showSnackBar(context, text: message),
      errorCallback: (message) => showSnackBar(context, text: message),
    );
    return _contact;
  }

  Future updateContact(
      BuildContext context, Contact contact, Function onSuccess) async {
    Contact _contact = await _apiService.updateContact(
      contact: contact,
      emptyCallback: (message) => showSnackBar(context, text: message),
      errorCallback: (message) => showSnackBar(context, text: message),
    );
    if (_contact != null) onSuccess();
  }

  Future<String> deleteContact(
    BuildContext context,
    Contact contact,
  ) async {
    String message = await _apiService.deleteContact(
      contactId: '${contact.id}',
      emptyCallback: (message) => showSnackBar(context, text: message),
      errorCallback: (message) => showSnackBar(context, text: message),
    );
    if (message != null) {
      await onRefreshList(context);
    }

    return message;
  }
}
