part of 'pages.dart';

class AddPage extends StatefulWidget {
  @override
  _AddPageState createState() => _AddPageState();
}

class _AddPageState extends State<AddPage> {
  TextEditingController _namaCtrl = TextEditingController();
  TextEditingController _emailCtrl = TextEditingController();
  TextEditingController _nohpCtrl = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final _base = Provider.of<BaseVM>(context);
    final _contactProvider = Provider.of<ContactVM>(context);

    return GeneralPage(
      title: 'Buat kontak',
      leftIcon: Icon(
        Icons.arrow_back_ios,
        color: Colors.white,
        size: defaultIconSize,
      ),
      onLeftIconTap: () {
        _base.goBack();
      },
      child: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(defaultMargin),
              children: [
                SizedBox(
                  height: 50,
                ),
                Container(
                  width: double.infinity,
                  height: 58,
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: TextFormField(
                    controller: _namaCtrl,
                    validator: (value) {
                      return value.isEmpty ? 'tolong isi' : null;
                    },
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(defaultMargin),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(color: mainColor, width: 1.5),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                              color: Colors.black.withOpacity(0.5), width: 1.0),
                        ),
                        hintText: 'Nama Lengkap'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: double.infinity,
                  height: 58,
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: TextFormField(
                    controller: _emailCtrl,
                    validator: (value) {
                      return value.isEmpty ? 'tolong isi' : null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(defaultMargin),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(color: mainColor, width: 1.5),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                              color: Colors.black.withOpacity(0.5), width: 1.0),
                        ),
                        hintText: 'Email'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: double.infinity,
                  height: 58,
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: TextFormField(
                    controller: _nohpCtrl,
                    validator: (value) {
                      return value.isEmpty ? 'tolong isi' : null;
                    },
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(defaultMargin),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(color: mainColor, width: 1.5),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                              color: Colors.black.withOpacity(0.5), width: 1.0),
                        ),
                        hintText: 'No Handphone'),
                  ),
                ),
                SizedBox(
                  height: 100,
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.all(defaultMargin),
              child: ElevatedButton(
                onPressed: _base.busy
                    ? () {}
                    : () async {
                        if (_formKey.currentState.validate()) {
                          _base.setBusy(true);
                          Contact _input = Contact(
                            id: 0,
                            email: _emailCtrl.text,
                            name: _namaCtrl.text,
                            nohp: _nohpCtrl.text,
                          );
                          Contact _output = await _contactProvider.addContact(
                              context, _input);
                          _base.setBusy(false);
                          if (_output != null) {
                            _contactProvider.goBack();
                            await _contactProvider.onRefreshList(context);
                          }
                        } else {
                          _base.showSnackBar(context,
                              text: 'Pastikan semua input terisi');
                        }
                      },
                style: ButtonStyle(
                  padding:
                      MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(10)),
                  backgroundColor: MaterialStateProperty.all<Color>(mainColor),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      side: BorderSide(
                        color: mainColor,
                      ),
                    ),
                  ),
                ),
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 300),
                  height: 35,
                  width: _base.busy ? 35 : 120,
                  child: _base.busy
                      ? SpinKitDoubleBounce(
                          color: Colors.white,
                          size: defaultIconSize,
                        )
                      : Text(
                          'Save',
                          style: whiteLabelButton,
                          textAlign: TextAlign.center,
                        ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
