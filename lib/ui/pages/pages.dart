import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gictestapp/shared/shared.dart';
import 'package:gictestapp/ui/widgets/widgets.dart';
import 'package:gictestapp/viewmodels/viewmodels.dart';
import 'package:gictestapp/models/models.dart';

import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

part 'general_page.dart';
part 'home_page.dart';
part 'detail_page.dart';
part 'update_page.dart';
part 'add_page.dart';
