part of 'pages.dart';

class DetailPage extends StatefulWidget {
  final int contactId;
  DetailPage(this.contactId);
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  Contact _details;
  int _n = 1;
  void minus() {
    setState(() {
      if (_n != 1) _n--;
    });
  }

  void add() {
    setState(() {
      _n++;
    });
  }

  getContact(int contactId) async {
    final _base = Provider.of<BaseVM>(context, listen: false);
    final _contactProvider = Provider.of<ContactVM>(context, listen: false);
    _base.setBusy(true);
    Contact _contact =
        await _contactProvider.getContact(context, widget.contactId);
    setState(() {
      _details = _contact;
    });
    _base.setBusy(false);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getContact(widget.contactId);
    });
  }

  @override
  Widget build(BuildContext context) {
    final _base = Provider.of<BaseVM>(context);
    final _contactProvider = Provider.of<ContactVM>(context);
    return GeneralPage(
      title: 'Details',
      leftIcon: Icon(
        Icons.arrow_back_ios,
        color: Colors.white,
        size: defaultIconSize,
      ),
      onLeftIconTap: () {
        _base.goBack();
      },
      child: _base.busy || _details == null
          ? Center(
              child: SpinKitSpinningCircle(
                color: mainColor,
                size: defaultIconSize,
              ),
            )
          : Stack(
              children: [
                ListView(
                  padding: EdgeInsets.all(defaultMargin),
                  children: [
                    SizedBox(
                      height: 50,
                    ),
                    Container(
                      width: double.infinity,
                      padding:
                          EdgeInsets.symmetric(horizontal: defaultMargin * 3),
                      child: Stack(
                        children: [
                          SpinKitDoubleBounce(
                            color: mainColor,
                            size: defaultIconSize,
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: FadeInImage.memoryNetwork(
                                placeholder: kTransparentImage,
                                fit: BoxFit.cover,
                                image:
                                    'https://picsum.photos/1${_details.id}/300',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Text(
                      _details.name,
                      style: blackFontStyle1.copyWith(
                        height: 1.3,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      _details.email,
                      style: greyFontStyle3.copyWith(
                        height: 1.3,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      _details.nohp,
                      style: greyFontStyle3.copyWith(
                        height: 1.3,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 100,
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Container(
                    margin: EdgeInsets.all(defaultMargin),
                    child: ElevatedButton(
                      onPressed: () {
                        _base.to(UpdatePage(_details, getContact));
                      },
                      style: ButtonStyle(
                        padding: MaterialStateProperty.all<EdgeInsets>(
                            EdgeInsets.all(15)),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(mainColor),
                        shape: MaterialStateProperty.all<CircleBorder>(
                          CircleBorder(
                            side: BorderSide(
                              color: mainColor,
                            ),
                          ),
                        ),
                      ),
                      child: Icon(
                        Icons.edit,
                        color: Colors.white,
                        size: defaultIconSize,
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Container(
                    margin: EdgeInsets.all(defaultMargin),
                    child: ElevatedButton(
                      onPressed: () async {
                        _base.showCustomDialog(
                            title: 'Konfirmasi',
                            description: 'Hapus ${_details.name} ?',
                            context: context,
                            labelSubmit: 'Hapus',
                            colorSubmit: Colors.red[400],
                            labelCancel: 'batalkan',
                            onSubmit: () async {
                              _base.goBack();
                              _base.setBusy(true);
                              String _message = await _contactProvider
                                  .deleteContact(context, _details);
                              _base.setBusy(false);
                              if (_message != null) {
                                _base.off(HomePage());
                              }
                            });
                      },
                      style: ButtonStyle(
                        padding: MaterialStateProperty.all<EdgeInsets>(
                            EdgeInsets.all(15)),
                        backgroundColor: MaterialStateProperty.all<Color>(
                          Colors.red[400],
                        ),
                        shape: MaterialStateProperty.all<CircleBorder>(
                          CircleBorder(
                            side: BorderSide(
                              color: Colors.red[400],
                            ),
                          ),
                        ),
                      ),
                      child: Icon(
                        Icons.delete,
                        color: Colors.white,
                        size: defaultIconSize,
                      ),
                    ),
                  ),
                )
              ],
            ),
    );
  }
}
