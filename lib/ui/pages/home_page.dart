part of 'pages.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _contactProvider = Provider.of<ContactVM>(context);
    final scaffoldkey = GlobalKey<ScaffoldState>();

    return StatefulWrapper(
      onInit: () async {
        _contactProvider.getContactList(context);
      },
      child: GeneralPage(
        scaffoldkey: scaffoldkey,
        drawer: DrawerApp(),
        leftIcon: Icon(
          Icons.menu_open_rounded,
          color: Colors.white,
          size: defaultIconSize,
        ),
        onLeftIconTap: () {
          scaffoldkey.currentState.openDrawer();
        },
        title: 'Daftar kontak',
        child: Stack(
          children: [
            PaginationListView(
              busy: _contactProvider.listContact == null,
              isError: _contactProvider.isError,
              isEnd: _contactProvider.endPage != null,
              itemPerPage: _contactProvider.countOfPages,
              itemBuilder: (context, index) {
                Contact contact = _contactProvider.listContact[index];
                return ContactItem(contact);
              },
              itemCount: _contactProvider.listContact?.length ?? 0,
              onRefresh: () => _contactProvider.onRefreshList(
                context,
              ),
              onLoad: (loaded) => _contactProvider
                  .onLoadNextContact(
                    context,
                  )
                  .then((a) => loaded()),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: EdgeInsets.all(defaultMargin),
                child: ElevatedButton(
                  onPressed: () {
                    _contactProvider.to(AddPage());
                  },
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all<EdgeInsets>(
                        EdgeInsets.all(15)),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(mainColor),
                    shape: MaterialStateProperty.all<CircleBorder>(
                      CircleBorder(
                        side: BorderSide(
                          color: mainColor,
                        ),
                      ),
                    ),
                  ),
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                    size: defaultIconSize,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
