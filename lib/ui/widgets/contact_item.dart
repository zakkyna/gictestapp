part of 'widgets.dart';

class ContactItem extends StatelessWidget {
  final Contact contact;
  ContactItem(this.contact);
  @override
  Widget build(BuildContext context) {
    final _base = Provider.of<BaseVM>(context, listen: false);
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          _base.to(DetailPage(contact.id));
        },
        child: Container(
          padding: EdgeInsets.symmetric(
              horizontal: defaultMargin, vertical: defaultMargin / 2),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 2,
                    child: Container(
                      padding: EdgeInsets.only(right: 24),
                      height: 120,
                      child: Stack(
                        children: [
                          SpinKitDoubleBounce(
                            color: mainColor,
                            size: defaultIconSize,
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: FadeInImage.memoryNetwork(
                                placeholder: kTransparentImage,
                                fit: BoxFit.cover,
                                image:
                                    'https://picsum.photos/1${contact.id}/300',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          contact.name,
                          maxLines: 2,
                          style: blackFontStyle2.copyWith(
                              height: 1.3, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          contact.email,
                          maxLines: 3,
                          style: greyFontStyle4.copyWith(height: 1.3),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          padding:
                              EdgeInsets.symmetric(vertical: 2, horizontal: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(20),
                            ),
                            border: Border.all(color: greyColor, width: 0.5),
                          ),
                          child: Text(
                            contact.nohp,
                            maxLines: 1,
                            style: blackFontStyle4.copyWith(
                              color: greyColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Divider(
                height: 1,
                color: greyColor,
              )
            ],
          ),
        ),
      ),
    );
  }
}
