part of 'widgets.dart';

class ContainerCard extends StatelessWidget {
  final Widget child;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  ContainerCard({this.child, this.margin, this.padding});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: padding == null
          ? EdgeInsets.symmetric(
              horizontal: defaultMargin,
              vertical: defaultMargin,
            )
          : padding,
      margin: margin == null
          ? EdgeInsets.symmetric(
              horizontal: defaultMargin,
              vertical: 8,
            )
          : margin,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: mainColor,
            offset: Offset(0.0, 0.8), //(x,y)
            blurRadius: 1.5,
          ),
        ],
      ),
      child: child,
    );
  }
}
