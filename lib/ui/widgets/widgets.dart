import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gictestapp/models/models.dart';
import 'package:gictestapp/ui/pages/pages.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:gictestapp/shared/shared.dart';
import 'package:gictestapp/viewmodels/viewmodels.dart';

part 'custom_dialog.dart';
part 'container_card.dart';
part 'pagination_listview.dart';
part 'contact_item.dart';
part 'stateful_wrapper.dart';
part 'drawer.dart';
