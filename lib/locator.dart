import 'package:get_it/get_it.dart';
import 'package:gictestapp/services/services.dart';
import 'package:gictestapp/viewmodels/viewmodels.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => BaseVM());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => APIService());
  locator.registerLazySingleton(() => ContactVM());
}
