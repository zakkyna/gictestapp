part of 'models.dart';

class Contact {
  int id;
  String name;
  String email;
  String nohp;

  Contact({
    this.id,
    this.name,
    this.email,
    this.nohp,
  });

  Contact.fromMap(Map snapshot)
      : id = snapshot['id'].runtimeType == int
            ? snapshot['id']
            : int.parse(snapshot['id']),
        name = snapshot['name'],
        email = snapshot['email'],
        nohp = snapshot['nohp'];

  toJson() {
    return {
      'id': id.toString(),
      'name': name,
      'email': email,
      'nohp': nohp,
    };
  }
}
