part of 'services.dart';

class APIService {
  final String baseUrl = 'https://gictest.herokuapp.com';
  Client client = Client();

  Future<List<Contact>> getContactList({
    @required String start,
    @required String limit,
    Function(String message) errorCallback,
    Function(String message) emptyCallback,
    Function(String message) successCallback,
  }) async {
    try {
      final response = await client.get(
        '$baseUrl/daftar?start=$start&limit=$limit',
      );
      print(response.body);
      final _response = json.decode(response.body);

      if (response.statusCode == 200) {
        List _data = _response;
        if (_data.isEmpty) {
          if (emptyCallback != null) {
            emptyCallback('Empty');
          }
          return [];
        } else {
          if (successCallback != null) {
            successCallback('Success');
          }
          List<Contact> _list =
              List<Contact>.from(_data.map((item) => Contact.fromMap(item)));
          return _list;
        }
      } else {
        if (errorCallback != null) {
          errorCallback('Error');
        }
        return null;
      }
    } catch (e) {
      print(e);
      if (errorCallback != null) {
        errorCallback(e.toString());
      }
      return null;
    }
  }

  Future<Contact> getContactFromId({
    @required int contactId,
    Function(String message) errorCallback,
    Function(String message) emptyCallback,
    Function(String message) successCallback,
  }) async {
    try {
      final response = await client.get(
        '$baseUrl/kontak/$contactId',
      );
      print(response.body);
      final _response = json.decode(response.body);

      if (response.statusCode == 200) {
        Map _data = _response;
        if (_data.isEmpty) {
          if (emptyCallback != null) {
            emptyCallback('Empty');
          }
          return null;
        } else {
          if (successCallback != null) {
            successCallback('Success');
          }
          Contact contact = Contact.fromMap(_data);
          return contact;
        }
      } else {
        if (errorCallback != null) {
          errorCallback('Error');
        }
        return null;
      }
    } catch (e) {
      print(e);
      if (errorCallback != null) {
        errorCallback(e.toString());
      }
      return null;
    }
  }

  Future<Contact> getContact({
    @required int contactId,
    Function(String message) errorCallback,
    Function(String message) emptyCallback,
    Function(String message) successCallback,
  }) async {
    try {
      final response = await client.get(
        '$baseUrl/kontak/$contactId',
      );
      print(response.body);
      final _response = json.decode(response.body);

      if (response.statusCode == 200) {
        Map _data = _response;
        if (_data.isEmpty) {
          if (emptyCallback != null) {
            emptyCallback('Empty');
          }
          return null;
        } else {
          if (successCallback != null) {
            successCallback('Success');
          }
          Contact contact = Contact.fromMap(_data);
          return contact;
        }
      } else {
        if (errorCallback != null) {
          errorCallback('Error');
        }
        return null;
      }
    } catch (e) {
      print(e);
      if (errorCallback != null) {
        errorCallback(e.toString());
      }
      return null;
    }
  }

  Future<Contact> addContact({
    @required Contact contact,
    Function(String message) errorCallback,
    Function(String message) emptyCallback,
    Function(String message) successCallback,
  }) async {
    try {
      final response = await client.post(
        '$baseUrl/buat',
        body: contact.toJson(),
        encoding: Encoding.getByName("utf-8"),
      );
      print(response.body);
      final _response = json.decode(response.body);

      if (response.statusCode == 200) {
        Map _data = _response;
        if (_data.isEmpty) {
          if (emptyCallback != null) {
            emptyCallback('Empty');
          }
          return null;
        } else {
          if (successCallback != null) {
            successCallback('Success');
          }
          Contact contact = Contact.fromMap(_data);
          return contact;
        }
      } else {
        if (errorCallback != null) {
          errorCallback('Error');
        }
        return null;
      }
    } catch (e) {
      print(e);
      if (errorCallback != null) {
        errorCallback(e.toString());
      }
      return null;
    }
  }

  Future<Contact> updateContact({
    @required Contact contact,
    Function(String message) errorCallback,
    Function(String message) emptyCallback,
    Function(String message) successCallback,
  }) async {
    try {
      final response = await client.put(
        '$baseUrl/ubah/${contact.id}',
        body: contact.toJson(),
        encoding: Encoding.getByName("utf-8"),
      );
      print(response.body);
      final _response = json.decode(response.body);

      if (response.statusCode == 200) {
        Map _data = _response;
        if (_data.isEmpty) {
          if (emptyCallback != null) {
            emptyCallback('Empty');
          }
          return null;
        } else {
          if (successCallback != null) {
            successCallback('Success');
          }
          Contact contact = Contact.fromMap(_data);
          return contact;
        }
      } else {
        if (errorCallback != null) {
          errorCallback('Error');
        }
        return null;
      }
    } catch (e) {
      print(e);
      if (errorCallback != null) {
        errorCallback(e.toString());
      }
      return null;
    }
  }

  Future<String> deleteContact({
    @required String contactId,
    Function(String message) errorCallback,
    Function(String message) emptyCallback,
    Function(String message) successCallback,
  }) async {
    try {
      final response = await client.delete(
        '$baseUrl/hapus/$contactId',
      );
      print(response.body);
      final _response = json.decode(response.body);

      if (response.statusCode == 200) {
        String _data = _response['message'];
        if (successCallback != null) {
          successCallback(_data);
        }
        return _data;
      } else {
        if (errorCallback != null) {
          errorCallback('Error');
        }
        return null;
      }
    } catch (e) {
      print(e);
      if (errorCallback != null) {
        errorCallback(e.toString());
      }
      return null;
    }
  }
}
